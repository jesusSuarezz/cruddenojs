import { Client } from 'https://deno.land/x/postgres/mod.ts';
import { uuid } from ' https://deno.land/x/uuid/mod.ts';

import { Product } from '../types.ts';
import { dbCredentials } from '../config.ts';

// Init client
const client = new Client(dbCredentials);

let products: Product[] = [
	{
		id: '1',
		name: 'Product one',
		description: 'This is product one',
		price: 29.99,
	},
	{
		id: '2',
		name: 'Product two',
		description: 'This is product two',
		price: 29.99,
	},
	{
		id: '3',
		name: 'Product three',
		description: 'This is product three',
		price: 29.99,
	},
];

// @desc    Obtiene todos los productos
// @route   GET /api/v1/products

const getProducts = async ({ response }: { response: any }) => {
	try {
		await client.connect();
		let result: any = await client.queryArray`SELECT * FROM products;`;

		console.log(result);

		const products = new Array();

		/* result.rows.map((p) => {
			let obj: any = new Object();

			result.rowDescription.columns.map((el, i) => {
				obj[el.name] = p[i];
			});

			products.push(obj);
		}); */

		response.status = 200;
		response.body = {
			success: true,
			data: products,
		};
	} catch (error) {
		response.status = 500;
		response.body = {
			success: false,
			msg: error.toString(),
		};
	} finally {
		await client.end();
	}
};

// @desc    Obtiene solo un producto
// @route   GET /api/v1/products/:id
const getProduct = ({
	params,
	response,
}: {
	params: { id: string };
	response: any;
}) => {
	const product: Product | undefined = products.find((p) => p.id === params.id);

	if (product) {
		response.status = 200;
		response.body = {
			success: true,
			data: product,
		};
	} else {
		response.status = 404;
		response.body = {
			success: false,
			msg: 'Producto no encontrado',
		};
	}
};

// @desc    Añadir un producto
// @route   POST /api/v1/products
const addProduct = async ({
	request,
	response,
}: {
	request: any;
	response: any;
}) => {
	const product = await request.body().value;
	const { name, description, price } = product;

	if (!request.hasBody) {
		response.status = 400;
		response.body = {
			success: false,
			msg: 'Sin datos',
		};
	} else {
		try {
			await client.connect();

			const result =
				await client.queryArray`INSERT INTO products(name,description,price) VALUES(${name},${description},${price})`;

			console.log(result.query.args);
			response.status = 201;
			response.body = {
				success: true,
				data: product,
			};
		} catch (error) {
			response.status = 500;
			response.body = {
				succes: false,
				msg: error.toString(),
			};
		} finally {
			await client.end();
		}
	}
};

// @desc    Actualizar un producto
// @route   PUT /api/v1/productS/:id
const updateProduct = async ({
	params,
	request,
	response,
}: {
	params: { id: string };
	request: any;
	response: any;
}) => {
	const product: Product | undefined = products.find((p) => p.id === params.id);

	const body = await request.body().value;
	//console.log(body);
	if (product) {
		const updateData: {
			name?: string;
			description?: string;
			price?: number;
		} = body;

		products = products.map((p) =>
			p.id === params.id ? { ...p, ...updateData } : p
		);

		response.status = 200;
		response.body = { success: true, data: products };
	} else {
		response.status = 404;
		response.body = {
			success: false,
			msg: 'Producto no encontrado',
		};
	}
};

// @desc    Actualizar un producto
// @route   PUT /api/v1/productS/:id
const deleteProduct = ({
	params,
	response,
}: {
	params: { id: string };
	response: any;
}) => {
	products = products.filter((p) => p.id !== params.id);
	response.body = {
		success: true,
		msg: 'Product removed',
	};
};

export { addProduct, deleteProduct, getProduct, getProducts, updateProduct };
